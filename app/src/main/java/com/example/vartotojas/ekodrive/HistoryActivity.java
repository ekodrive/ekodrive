package com.example.vartotojas.ekodrive;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class HistoryActivity extends AppCompatActivity {

    private TextView text01,text02,text03;
    private TextView text11,text12,text13;
    private TextView text21,text22,text23;
    private TextView text31,text32,text33;
    private TextView text41,text42,text43;
    private TextView text51,text52,text53;
    private TextView text61,text62,text63;
    private TextView text71,text72,text73;
    private TextView text81,text82,text83;
    private TextView text91,text92,text93;
    private TextView text101,text102,text103;
    private  TextView neraDuomenu;

    private Intent mainActivityIntent;
    private Intent settingsActivityIntent;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    if(mainActivityIntent==null){
                        startActivity(new Intent(HistoryActivity.this, MainActivity.class));
                    }
                    else {
                        startActivity(mainActivityIntent);
                    }
                    //startActivity(new Intent(HistoryActivity.this, MainActivity.class));
                    return true;
                case R.id.navigation_statistics:

                    return true;
                case R.id.navigation_settings:
                    if(settingsActivityIntent==null){
                        startActivity(new Intent(HistoryActivity.this,SettingsActivity.class));
                    }
                    else {
                        startActivity(settingsActivityIntent);
                    }

                    //startActivity(new Intent(HistoryActivity.this, SettingsActivity.class));
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);


        neraDuomenu=(TextView)findViewById(R.id.neraDuomenu);
        text01=(TextView)findViewById(R.id.balai);
        text02=(TextView)findViewById(R.id.atstumas);
        text11=(TextView)findViewById(R.id.balai1);
        text12=(TextView)findViewById(R.id.atstumas1);
        text21=(TextView)findViewById(R.id.balai2);
        text22=(TextView)findViewById(R.id.atstumas2);
        text31=(TextView)findViewById(R.id.balai3);
        text32=(TextView)findViewById(R.id.atstumas3);
        text41=(TextView)findViewById(R.id.balai4);
        text42=(TextView)findViewById(R.id.atstumas4);
        text51=(TextView)findViewById(R.id.balai5);
        text52=(TextView)findViewById(R.id.atstumas5);
        text61=(TextView)findViewById(R.id.balai6);
        text62=(TextView)findViewById(R.id.atstumas6);
        text71=(TextView)findViewById(R.id.balai7);
        text72=(TextView)findViewById(R.id.atstumas7);
        text81=(TextView)findViewById(R.id.balai8);
        text82=(TextView)findViewById(R.id.atstumas8);
        text91=(TextView)findViewById(R.id.balai9);
        text92=(TextView)findViewById(R.id.atstumas9);
        text101=(TextView)findViewById(R.id.balai10);
        text102=(TextView)findViewById(R.id.atstumas10);
        text03=(TextView)findViewById(R.id.nr);
        text13=(TextView)findViewById(R.id.nr1);
        text23=(TextView)findViewById(R.id.nr2);
        text33=(TextView)findViewById(R.id.nr3);
        text43=(TextView)findViewById(R.id.nr4);
        text53=(TextView)findViewById(R.id.nr5);
        text63=(TextView)findViewById(R.id.nr6);
        text73=(TextView)findViewById(R.id.nr7);
        text83=(TextView)findViewById(R.id.nr8);
        text93=(TextView)findViewById(R.id.nr9);
        text103=(TextView)findViewById(R.id.nr10);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_statistics);


        Intent intent=getIntent();
        int [] points =intent.getIntArrayExtra("points");
        double[] roads=intent.getDoubleArrayExtra("roads");

        mainActivityIntent =new Intent(this, MainActivity.class);
        settingsActivityIntent= new Intent(this,SettingsActivity.class);
        mainActivityIntent.putExtra("points",points);
        mainActivityIntent.putExtra("roads",roads);
        settingsActivityIntent.putExtra("points",points);
        settingsActivityIntent.putExtra("roads",roads);


        text12.setText(Math.floor(roads[0])/1000+"");
        text22.setText(Math.floor(roads[1])/1000+"");
        text32.setText(Math.floor(roads[2])/1000+"");
        text42.setText(Math.floor(roads[3])/1000+"");
        text52.setText(Math.floor(roads[4])/1000+"");
        text62.setText(Math.floor(roads[5])/1000+"");
        text72.setText(Math.floor(roads[6])/1000+"");
        text82.setText(Math.floor(roads[7])/1000+"");
        text92.setText(Math.floor(roads[8])/1000+"");
        text102.setText(Math.floor(roads[9])/1000+"");

        long answer;
        long ptsAvailable,sum;

        ptsAvailable=Math.round(Math.floor(roads[0])/1000*10);
        sum=ptsAvailable-points[0];
        if(sum<=0||ptsAvailable==0) answer=0;
        else answer=Math.round((sum*10)/ptsAvailable);
        text11.setText(answer+"");

        ptsAvailable=Math.round(Math.floor(roads[1])/1000*10);
        sum=ptsAvailable-points[1];
        if(sum<=0||ptsAvailable==0) answer=0;
        else answer=Math.round((sum*10)/ptsAvailable);
        text21.setText(answer+"");

        ptsAvailable=Math.round(Math.floor(roads[2])/1000*10);
        sum=ptsAvailable-points[2];
        if(sum<=0||ptsAvailable==0) answer=0;
        else answer=Math.round((sum*10)/ptsAvailable);
        text31.setText(answer+"");

        ptsAvailable=Math.round(Math.floor(roads[3])/1000*10);
        sum=ptsAvailable-points[3];
        if(sum<=0||ptsAvailable==0) answer=0;
        else answer=Math.round((sum*10)/ptsAvailable);
        text41.setText(answer+"");

        ptsAvailable=Math.round(Math.floor(roads[4])/1000*10);
        sum=ptsAvailable-points[4];
        if(sum<=0||ptsAvailable==0) answer=0;
        else answer=Math.round((sum*10)/ptsAvailable);
        text51.setText(answer+"");

        ptsAvailable=Math.round(Math.floor(roads[5])/1000*10);
        sum=ptsAvailable-points[5];
        if(sum<=0||ptsAvailable==0) answer=0;
        else answer=Math.round((sum*10)/ptsAvailable);
        text61.setText(answer+"");

        ptsAvailable=Math.round(Math.floor(roads[6])/1000*10);
        sum=ptsAvailable-points[6];
        if(sum<=0||ptsAvailable==0) answer=0;
        else answer=Math.round((sum*10)/ptsAvailable);
        text71.setText(answer+"");

        ptsAvailable=Math.round(Math.floor(roads[7])/1000*10);
        sum=ptsAvailable-points[7];
        if(sum<=0||ptsAvailable==0) answer=0;
        else answer=Math.round((sum*10)/ptsAvailable);
        text81.setText(answer+"");

        ptsAvailable=Math.round(Math.floor(roads[8])/1000*10);
        sum=ptsAvailable-points[8];
        if(sum<=0||ptsAvailable==0) answer=0;
        else answer=Math.round((sum*10)/ptsAvailable);
        text91.setText(answer+"");

        ptsAvailable=Math.round(Math.floor(roads[9])/1000*10);
        sum=ptsAvailable-points[9];
        if(sum<=0||ptsAvailable==0) answer=0;
        else answer=Math.round((sum*10)/ptsAvailable);
        text101.setText(answer+"");


        int whereNull=0;
        for(int j=0;j<10;j++){
            if(roads[j]==0){
                whereNull++;
            }
        }

        if(whereNull==10){
            neraDuomenu.setText("Nėra surinkta pakankamai duomenų");
            text01.setText("");text02.setText("");text03.setText("");
            text11.setText("");text12.setText("");text13.setText("");
            text21.setText("");text22.setText("");text23.setText("");
            text31.setText("");text32.setText("");text33.setText("");
            text41.setText("");text42.setText("");text43.setText("");
            text51.setText("");text52.setText("");text53.setText("");
            text61.setText("");text62.setText("");text63.setText("");
            text71.setText("");text72.setText("");text73.setText("");
            text81.setText("");text82.setText("");text83.setText("");
            text91.setText("");text92.setText("");text93.setText("");
            text101.setText("");text102.setText("");text103.setText("");
        }
        if(whereNull==9){
            text21.setText("");text22.setText("");text23.setText("");
            text31.setText("");text32.setText("");text33.setText("");
            text41.setText("");text42.setText("");text43.setText("");
            text51.setText("");text52.setText("");text53.setText("");
            text61.setText("");text62.setText("");text63.setText("");
            text71.setText("");text72.setText("");text73.setText("");
            text81.setText("");text82.setText("");text83.setText("");
            text91.setText("");text92.setText("");text93.setText("");
            text101.setText("");text102.setText("");text103.setText("");
        }
        if(whereNull==8){
            text31.setText("");text32.setText("");text33.setText("");
            text41.setText("");text42.setText("");text43.setText("");
            text51.setText("");text52.setText("");text53.setText("");
            text61.setText("");text62.setText("");text63.setText("");
            text71.setText("");text72.setText("");text73.setText("");
            text81.setText("");text82.setText("");text83.setText("");
            text91.setText("");text92.setText("");text93.setText("");
            text101.setText("");text102.setText("");text103.setText("");
        }
        if(whereNull==7){
            text41.setText("");text42.setText("");text43.setText("");
            text51.setText("");text52.setText("");text53.setText("");
            text61.setText("");text62.setText("");text63.setText("");
            text71.setText("");text72.setText("");text73.setText("");
            text81.setText("");text82.setText("");text83.setText("");
            text91.setText("");text92.setText("");text93.setText("");
            text101.setText("");text102.setText("");text103.setText("");
        }
        if(whereNull==6){
            text51.setText("");text52.setText("");text53.setText("");
            text61.setText("");text62.setText("");text63.setText("");
            text71.setText("");text72.setText("");text73.setText("");
            text81.setText("");text82.setText("");text83.setText("");
            text91.setText("");text92.setText("");text93.setText("");
            text101.setText("");text102.setText("");text103.setText("");
        }
        if(whereNull==5){
            text61.setText("");text62.setText("");text63.setText("");
            text71.setText("");text72.setText("");text73.setText("");
            text81.setText("");text82.setText("");text83.setText("");
            text91.setText("");text92.setText("");text93.setText("");
            text101.setText("");text102.setText("");text103.setText("");
        }
        if(whereNull==4){
            text71.setText("");text72.setText("");text73.setText("");
            text81.setText("");text82.setText("");text83.setText("");
            text91.setText("");text92.setText("");text93.setText("");
            text101.setText("");text102.setText("");text103.setText("");
        }
        if(whereNull==3){
            text81.setText("");text82.setText("");text83.setText("");
            text91.setText("");text92.setText("");text93.setText("");
            text101.setText("");text102.setText("");text103.setText("");
        }
        if(whereNull==2){
            text91.setText("");text92.setText("");text93.setText("");
            text101.setText("");text102.setText("");text103.setText("");
        }
        if(whereNull==1){
            text101.setText("");text102.setText("");text103.setText("");
        }



    }

}