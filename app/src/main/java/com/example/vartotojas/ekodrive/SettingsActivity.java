package com.example.vartotojas.ekodrive;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import java.net.Inet4Address;

public class SettingsActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private Intent mainActivityIntent;
    private Intent historyActivityIntent;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    //startActivity(new Intent(SettingsActivity.this, MainActivity.class));
                    if(mainActivityIntent==null){
                        startActivity(new Intent(SettingsActivity.this, MainActivity.class));
                    }
                    else {
                        startActivity(mainActivityIntent);
                    }
                    return true;
                case R.id.navigation_statistics:
                    if(historyActivityIntent==null){
                        startActivity(new Intent(SettingsActivity.this,HistoryActivity.class));
                    }
                    else {
                        startActivity(historyActivityIntent);
                    }
                    //startActivity(new Intent(SettingsActivity.this, HistoryActivity.class));
                    return true;
                case R.id.navigation_settings:

                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Intent intent=getIntent();
        int [] points =intent.getIntArrayExtra("points");
        double[] roads=intent.getDoubleArrayExtra("roads");

        mainActivityIntent =new Intent(this, MainActivity.class);
        historyActivityIntent= new Intent(this,HistoryActivity.class);
        mainActivityIntent.putExtra("points",points);
        mainActivityIntent.putExtra("roads",roads);
        historyActivityIntent.putExtra("points",points);
        historyActivityIntent.putExtra("roads",roads);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_settings);
    }

}
