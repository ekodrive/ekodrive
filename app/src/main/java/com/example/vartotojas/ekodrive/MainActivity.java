package com.example.vartotojas.ekodrive;

import android.Manifest;
import android.content.Intent;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.media.ToneGenerator;
import android.media.AudioManager;
import android.content.SharedPreferences;
import android.app.Activity;


import com.github.lzyzsd.circleprogress.ArcProgress;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;

import static java.lang.Math.nextDown;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class MainActivity extends AppCompatActivity implements SensorEventListener, IBaseGpsListener {

    private TextView pagreitis, GPSpagreitis, testavimui, rezultatas;
    private Sensor mySensor;
    private SensorManager SM;
    private Button start;
    private int[] points = new int[10];
    private double[] road = new double[10];
    private int index;
    private Intent historyActivityIntent;
    private Intent settingsActivityIntent;
    private int buttonCnt;
    private boolean ifCnt;

    private float[] gravity = new float[3];
    private float[] linear_acceleration = new float[3];
    private List<Double> aList = new ArrayList<Double>();

    private ArcProgress speedProgBar;
    private double speed;
    private long time;
    private double roadS;
    private int pts;

    ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);


    @Override
    public void onSensorChanged(SensorEvent event) {
        // metodas vykdomas sensoriui padodant naujus duomenis

        final float alpha = 0.8f; // alpha = t / (t+ dT), kai t laiko konstanta, o dT gaunamos info daznis

        // Skaiciuojama gravitacija visoms asims
        gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
        gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
        gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

        // Gravitacija panaikinama nuo kiekvienos asies norint gauti linijini pagreiti
        linear_acceleration[0] = event.values[0] - gravity[0];
        linear_acceleration[1] = event.values[1] - gravity[1];
        linear_acceleration[2] = event.values[2] - gravity[2];

        // apskaiciuojamas pagreitis
        double a = sqrt(pow(linear_acceleration[0], 2) + pow(linear_acceleration[1], 2) + pow(linear_acceleration[2], 2));
        aList.add(a);

        if (aList.size() == 20) {
            double sum = 0;
            Collections.sort(aList);
            for (int i = 5; i <= 15; i++)
                sum += aList.get(i);

            sum /= 10;
            a = sum;
            aList.clear();
            a = Math.floor(a * 100) / 100; // skaiciai po kablelio
        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //nenaudojamas, bet turi buti aprasytas

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int[] ptsArray=getArray();
        int cntt=0;
        for(int p:ptsArray){
            points[cntt++]=p;
        }
        cntt=0;
        double[] roadArray=getArraydouble();
        for(double s:roadArray){
            road[cntt++]=s;
        }

        buttonCnt = 0;
        getSupportActionBar().hide();

        SM = (SensorManager) getSystemService(SENSOR_SERVICE); //Sukuriam objekta proceso valdymui(sensor manager)

        mySensor = SM.getDefaultSensor(Sensor.TYPE_ACCELEROMETER); //Kuriam sensoriu

        SM.registerListener(this, mySensor, SensorManager.SENSOR_DELAY_NORMAL); // registruojame sensoriaus registravima

        // apsirasome tekstus
        //pagreitis = (TextView) findViewById(R.id.pagreitis);
        //GPSpagreitis = (TextView) findViewById(R.id.GPSpagreitis);
        //testavimui = (TextView) findViewById(R.id.testavimui);
        //testas= (TextView)findViewById(R.id.textView);
        rezultatas = (TextView) findViewById(R.id.Rezultatas);

        //inicializacija GPS pagreicio skaiciavimui
        time = 0;
        speed = 0;
        roadS = 0;
        pts = 0;
        index = 0;
        /*Bundle b = new Bundle();
        b.putIntArray("INT_ARRAY",points);
        Intent i =new Intent(MainActivity.this,HistoryActivity.class);
        i.putExtras(b);*/

        historyActivityIntent = new Intent(this, HistoryActivity.class);
        historyActivityIntent.putExtra("points", points);
        historyActivityIntent.putExtra("roads", road);

        settingsActivityIntent = new Intent(this, SettingsActivity.class);
        settingsActivityIntent.putExtra("points", points);
        settingsActivityIntent.putExtra("roads", road);


        final Intent intent = getIntent();
        int[] pointsIntent = intent.getIntArrayExtra("points");
        final double[] roadsIntents = intent.getDoubleArrayExtra("roads");

        int count = 0;
        if (pointsIntent != null) {
            for (int p : pointsIntent) {
                points[count] = p;
                count++;
            }
        }
        count = 0;
        if (roadsIntents != null) {
            for (double r : roadsIntents) {
                road[count] = r;
                count++;
            }
        }
        speedProgBar = (ArcProgress) findViewById(R.id.spidometras); // Spidometro vizualizacija

        // greiciui

        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

        this.updateSpeedKmh(null); // atnaujina greiti

        // bottom nav bar'as
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        for (int p : points) {
            if (p != 0) {
                index++;
            }
        }

        start = (Button) findViewById(R.id.start_button);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (buttonCnt % 2 == 1) {
                    start.setText("Pradėti skaičiavimus");

                    if (Math.floor(roadS) / 1000 >= 1) {
                        for (int i = points.length - 2; i >= 0; i--) {
                            points[i + 1] = points[i];
                            road[i + 1] = road[i];
                        }
                        points[0] = pts;
                        road[0] = roadS;
                        long answer;
                        long ptsAvailable = Math.round(Math.floor(roadS) / 1000 * 10);
                        long sum = ptsAvailable - pts;
                        if (sum <= 0) answer = 0;
                        else answer = Math.round((sum * 10) / ptsAvailable);
                        rezultatas.setText("Balai: " + answer);
                    } else {
                        rezultatas.setText("Reikia nuvažiuoti bent 1 km");
                    }

                    ifCnt=false;
                    index++;

                } else {
                    start.setText("Baigti skaičiavimus");
                    roadS = 0;
                    pts = 0;
                    rezultatas.setText("");
                    ifCnt=true;

                }
                buttonCnt++;
            }
        });


    }

    public boolean saveArray() {
        SharedPreferences sp = this.getSharedPreferences("array", Activity.MODE_PRIVATE);
        SharedPreferences.Editor mEdit1 = sp.edit();
        mEdit1.putInt("int0",points[0]);
        mEdit1.putInt("int1",points[1]);
        mEdit1.putInt("int2",points[2]);
        mEdit1.putInt("int3",points[3]);
        mEdit1.putInt("int4",points[4]);
        mEdit1.putInt("int5",points[5]);
        mEdit1.putInt("int6",points[6]);
        mEdit1.putInt("int7",points[7]);
        mEdit1.putInt("int8",points[8]);
        mEdit1.putInt("int9",points[9]);
        return mEdit1.commit();
    }

    public int[] getArray() {
        SharedPreferences sp = this.getSharedPreferences("array", Activity.MODE_PRIVATE);

        //NOTE: if shared preference is null, the method return empty Hashset and not null
        int point[]=new int[10];
        point[0]=sp.getInt("int0",-1);
        point[1]=sp.getInt("int1",-1);
        point[2]=sp.getInt("int2",-1);
        point[3]=sp.getInt("int3",-1);
        point[4]=sp.getInt("int4",-1);
        point[5]=sp.getInt("int5",-1);
        point[6]=sp.getInt("int6",-1);
        point[7]=sp.getInt("int7",-1);
        point[8]=sp.getInt("int8",-1);
        point[9]=sp.getInt("int9",-1);
        return point;
    }

    public boolean saveArraydouble() {
        SharedPreferences sp = this.getSharedPreferences("array", Activity.MODE_PRIVATE);
        SharedPreferences.Editor mEdit1 = sp.edit();
        mEdit1.putLong("double0",Double.doubleToRawLongBits(road[0]));
        mEdit1.putLong("double1",Double.doubleToRawLongBits(road[1]));
        mEdit1.putLong("double2",Double.doubleToRawLongBits(road[2]));
        mEdit1.putLong("double3",Double.doubleToRawLongBits(road[3]));
        mEdit1.putLong("double4",Double.doubleToRawLongBits(road[4]));
        mEdit1.putLong("double5",Double.doubleToRawLongBits(road[5]));
        mEdit1.putLong("double6",Double.doubleToRawLongBits(road[6]));
        mEdit1.putLong("double7",Double.doubleToRawLongBits(road[7]));
        mEdit1.putLong("double8",Double.doubleToRawLongBits(road[8]));
        mEdit1.putLong("double9",Double.doubleToRawLongBits(road[9]));


        return mEdit1.commit();
    }

    public double[] getArraydouble() {
        SharedPreferences sp = this.getSharedPreferences("array", Activity.MODE_PRIVATE);

        double[] roadL=new double[10];
        roadL[0]=Double.longBitsToDouble(sp.getLong("double0",0));
        roadL[1]=Double.longBitsToDouble(sp.getLong("double1",0));
        roadL[2]=Double.longBitsToDouble(sp.getLong("double2",0));
        roadL[3]=Double.longBitsToDouble(sp.getLong("double3",0));
        roadL[4]=Double.longBitsToDouble(sp.getLong("double4",0));
        roadL[5]=Double.longBitsToDouble(sp.getLong("double5",0));
        roadL[6]=Double.longBitsToDouble(sp.getLong("double6",0));
        roadL[7]=Double.longBitsToDouble(sp.getLong("double7",0));
        roadL[8]=Double.longBitsToDouble(sp.getLong("double8",0));
        roadL[9]=Double.longBitsToDouble(sp.getLong("double9",0));

        return roadL;
    }

    @Override
    public void onStop() {
        saveArray();
        saveArraydouble();
        super.onStop();
    }



    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("index", index);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        int idx = savedInstanceState.getInt("index");
    }

    // Metodai greiciui

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return;
            }
        }
    }

    public void finish() {
        super.finish();
        System.exit(0);
    }

    // Pagal gauta vieta is GPS atnaujina greiti esamoje pozicijoje
    public void updateSpeedKmh(CLocation location) {
        float currentSpeed = 0;

        if (location != null) {
            currentSpeed = location.getSpeedKmh();
        }
        Formatter format = new Formatter(new StringBuilder());
        format.format(Locale.US, "%4.1f", currentSpeed);

        long temp = System.currentTimeMillis() / 1000 - 1493302347;
        long currTime = System.currentTimeMillis() / 1000; //- 1482192000;

        //testavimui.setText((int)currTime/3600);

        long timeDif = currTime - time;
        double speedDif = (currentSpeed - speed) / 3.6;
        // nuvaziuotas atstumas
        roadS += timeDif * currentSpeed / 3.6;

        double a = speedDif / timeDif; //(currentSpeed/3600 - speed) / (currTime - time);
        time = currTime;
        speed = currentSpeed;
        a = Math.floor(a * 100) / 100;
        double sv = Math.floor(roadS) / 1000;
        speedProgBar.setProgress((int) currentSpeed);
        if (Double.isNaN(a));
           // GPSpagreitis.setText("GPS a= 0.0 m/roadS²");
        else;
            //GPSpagreitis.setText("GPS a= " + a + " m/roadS²");

        String test1 = Double.toString(roadS / 1000);
        //pagreitis.setText("GPS roadS=" + sv + "km");

        if ((Double.compare(1.5, a) < 0 || Double.compare(-2.5, a) > 0) && Double.compare(100.0, a) > 0 && Double.compare(-100.0, a) < 0 && ifCnt) {
            toneGen1.startTone(ToneGenerator.TONE_CDMA_INTERCEPT, 220);
            pts++;
        }

        //testavimui.setText("Baudos taskai:" + Integer.toString(pts));

    }


    // Tikrina, jei vieta nera lygi null, tai atnaujina greiti
    public void onLocationChanged(Location location) {
        if (location != null) {
            CLocation myLocation = new CLocation(location, true);
            this.updateSpeedKmh(myLocation);
        }
    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onGpsStatusChanged(int event) {

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                    return true;
                case R.id.navigation_statistics:
                    if (historyActivityIntent == null) {
                        startActivity(new Intent(MainActivity.this, HistoryActivity.class));
                    } else {
                        startActivity(historyActivityIntent);
                    }

                    return true;
                case R.id.navigation_settings:
                    if (settingsActivityIntent == null) {
                        startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                    } else {
                        startActivity(settingsActivityIntent);
                    }
                    return true;
            }
            return false;
        }

    };

}